import jwt, { JwtPayload } from 'jsonwebtoken'
import { User } from '../model/user.model';

const SECRET_KEY_JWT = "supersecretkey";

function getNewToken(user: User): string {
    return jwt.sign({ name: user.name, email: user.email, role: user.role }, SECRET_KEY_JWT);
}

async function verityJwt(token: string): Promise<String | JwtPayload | null> {
    try {
        const decode = jwt.verify(token, SECRET_KEY_JWT);
        console.log("decode", decode);

        return decode
    } catch (err) {
        return null
    }

}

export { getNewToken, verityJwt }