import mongoose from 'mongoose'

const connectDb = async () => {
	return await mongoose.connect("mongodb://localhost:27017/needjobs");
};

export { connectDb };
