import { injectable } from "inversify";
import { Model } from "mongoose";
import { User, UserModel } from "../model/user.model";

@injectable()
export class UserRepo {

    private readonly userModel: Model<User, {}, {}, {}>


    constructor() {
        this.userModel = UserModel
    }

    async findAllUsers(): Promise<User[]> {
        return this.userModel.find({})
    }

    async createUser(user: User): Promise<User> {
        const doc = new UserModel(user);
        return await doc.save();
    }

    async isAValidUser(email: string, password: string): Promise<User | undefined | null> {
        const user = await this.userModel.findOne({ email: email })
        // @ts-ignore
        if (await user.comparePassword(password)) {
            return user
        } else {
            null
        }
    }
}