import { injectable } from "inversify";
import { Model } from "mongoose";
import { Job, JobModel } from "../model/job.model";

@injectable()
export class JobRepo {

    private readonly _jobModel: Model<Job, {}, {}, {}>


    constructor() {
        this._jobModel = JobModel
    }

    async finaAllJobs(): Promise<Job[]> {
        return this._jobModel.find({})
    }

    async createJob(job: Job): Promise<Job> {
        const doc = new JobModel(job);
        return await doc.save();
    }


}