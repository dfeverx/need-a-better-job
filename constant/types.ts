const TYPES = {
    UserService: Symbol.for('UserService'),
    UserRepo: Symbol.for('UserRepo'),
    JobService: Symbol.for('JobService'),
    JobRepo: Symbol.for('JobRepo'),
};

export default TYPES;