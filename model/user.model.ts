import { Schema, model, CallbackError } from 'mongoose';
import bcrypt from 'bcrypt';

const SALT_WORK_FACTOR = 10;
// 1. Create an interface representing a document in MongoDB.
export interface User {
    name: string;
    email: string;
    password: string;
    role: string;
}

// 2. Create a Schema corresponding to the document interface.
const schema = new Schema<User>({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String, required: true, default: 'user' }
});

schema.pre('save', function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err: CallbackError | undefined, salt: string | number) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);
            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

schema.methods.comparePassword = function (candidatePassword: string) {
    return bcrypt.compare(candidatePassword, this.password)
    /* bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    }); */
};

// 3. Create a Model.
export const UserModel = model<User>('User', schema);



