import { Schema, model, Types } from 'mongoose';



// 1. Create an interface representing a document in MongoDB.
export interface Job {
    jobName: string;
    description: string;
    jobType: string;
    user: Types.ObjectId;
}

// 2. Create a Schema corresponding to the document interface.
const schema = new Schema<Job>({
    jobName: { type: String, required: true },
    description: { type: String, required: true },
    jobType: { type: String, required: true, },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
});


// 3. Create a Model.
export const JobModel = model<Job>('Job', schema);



