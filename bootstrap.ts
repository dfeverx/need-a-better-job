import "reflect-metadata";
import mongoose from 'mongoose'
import express from 'express'
import * as bodyParser from 'body-parser';
import { Container } from 'inversify';
import { InversifyExpressServer } from 'inversify-express-utils';
import TYPES from './constant/types';

// declare metadata by @controller annotation
import "./controller/user.controller";
import "./controller/job.controller"

import { UserRepo } from './repository/user.repository';
import { UserService } from './service/user.service';
import { JobRepo } from "./repository/job.repository";
import { JobService } from "./service/job.service";

// set up container
let container = new Container();

// set up bindings
container.bind<UserRepo>(TYPES.UserRepo).to(UserRepo);
container.bind<UserService>(TYPES.UserService).to(UserService);
container.bind<JobRepo>(TYPES.JobRepo).to(JobRepo);
container.bind<JobService>(TYPES.JobService).to(JobService);

// create server
let server = new InversifyExpressServer(container);
server.setConfig((app) => {
    // add body parser
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(express.json());
});

let app = server.build();

const connectToMongoDb = async () => {
    return mongoose.connect("mongodb://localhost:27017/demo");
};


connectToMongoDb().then(() => {
    console.log("Connected to db..");
    app.listen(3000)

})
