import { inject, injectable } from "inversify";
import TYPES from "../constant/types";
import { Job } from "../model/job.model";
import { JobRepo } from "../repository/job.repository";

@injectable()
export class JobService {


    private readonly _jobRepo: JobRepo

    constructor(@inject(TYPES.JobRepo) jobRepo: JobRepo) {
        this._jobRepo = jobRepo
    }
    async findAllJobs() {
        const jobs = await this._jobRepo.finaAllJobs()
        const jsonRes = { respost: 200, data: jobs }
        return jsonRes
    }

    async createJob(job: Job): Promise<Job> {
        return this._jobRepo.createJob(job)
    }



}