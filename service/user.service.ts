import { inject, injectable } from "inversify";
import TYPES from "../constant/types";
import { User } from "../model/user.model";
import { UserRepo } from "../repository/user.repository";

@injectable()
export class UserService {


    private readonly _userRepos: UserRepo

    constructor(@inject(TYPES.UserRepo) userRepo: UserRepo) {
        this._userRepos = userRepo
    }
    async findAllUsers() {
        const users = await this._userRepos.findAllUsers()
        const jsonRes = { respost: 200, data: users }
        return jsonRes
    }

    async createUser(user: User): Promise<User> {
        return this._userRepos.createUser(user)
    }


    async isAValidUser(email: string, password: string): Promise<User | null | undefined> {
        const user = this._userRepos.isAValidUser(email, password)
        return user

    }
}