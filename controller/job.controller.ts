import * as express from "express";
import { inject } from "inversify";
import { controller, httpGet, httpPost, next, request, response } from "inversify-express-utils";
import TYPES from "../constant/types";
import { verityJwt } from "../helper/jwt";
import { JobService } from "../service/job.service";

@controller('/job')
export class JobController {
    private readonly _jobService: JobService;

    constructor(@inject(TYPES.JobService) jobService: JobService) {
        this._jobService = jobService
    }

    @httpGet('/public')
    async allJobs(@request() req: express.Request) {
        console.log(req.body);
        const outRes = await this._jobService.findAllJobs()
        return outRes

    }

    @httpPost("/create")
    async createJobPost(@request() request: express.Request) {
        const job = request.body
        if (request.headers.authorization) {
            const token = request.headers.authorization.split(' ')[1];
            const payload = await verityJwt(token);
            // @ts-ignore
            if (payload != null && payload.role === 'company-admin') {
                const out = await this._jobService.createJob(job)
                return out

            } else {
                throw new Error("Unauthorized");
            }
        } else {
            throw new Error("Unauthorized");
        }
    }

}