import * as express from "express";
import { inject } from "inversify";
import { controller, httpPost, next, request, response } from "inversify-express-utils";
import TYPES from "../constant/types";
import { getNewToken } from "../helper/jwt";
import { UserService } from "../service/user.service";

@controller('/user')
export class UserController {
    private readonly _userService: UserService;

    constructor(@inject(TYPES.UserService) userService: UserService) {
        this._userService = userService
    }

    @httpPost('/')
    async allUsers(@request() req: express.Request, @response() res: express.Response, @next() next: express.NextFunction) {
        console.log(req.body);
        const outRes = await this._userService.findAllUsers()
        return outRes

    }
    @httpPost('/signup')
    async signup(@request() req: express.Request) {
        const user = req.body
        // check passwords are same
        if (user.password == user.confirmPassword) {
            // check name and email are not empty
            if (user.name != "" && user.email != "") {
                await this._userService.createUser(user)
                return { msg: "User created successfully" }
            } else {
                throw new Error("Name and Email are required");
            }

        } else {
            throw new Error("Passwords do not match");
        }

    }
    @httpPost('/signin')
    async signin(@request() req: express.Request) {
        const user = req.body
        // validate name and email password are exist
        if (user.name != "" && user.email != "" && user.password != "") {
            const verifiedUser = await this._userService.isAValidUser(user.email, user.password)
            if (!verifiedUser) {
                throw new Error("User not found");

            }
            const token = getNewToken(verifiedUser)
            return { jwt: token }

        }
    }
}